from app import db


class Character(db.Model):
    char_id = db.Column(db.Integer, primary_key = True)
    char_name = db.Column(db.String(63))
    birthday = db.Column(db.String(63))
    cards = db.relationship("Card", cascade="all,delete", backref="character")
    band_id = db.Column(db.Integer, db.ForeignKey("band.band_id"))
    gacha_id = db.Column(db.Integer, db.ForeignKey("gacha.gacha_id"))


    def __repr__(self):
        return '<Character {}>'.format(self.char_name)

class Band(db.Model):
    band_id = db.Column(db.Integer, primary_key = True)
    band_name = db.Column(db.String(63))
    character = db.relationship("Character", backref="band")
    songs = db.relationship("Song", backref="band")

    def __repr__(self):
        return '<Band {}>'.format(self.band_name)

class Song(db.Model):
    song_id = db.Column(db.Integer, primary_key = True)
    song_title = db.Column(db.String(63))
    song_length = db.Column(db.String(63))
    ex_level = db.Column(db.Integer)
    note_count = db.Column(db.Integer)
    song_art = db.Column(db.String(255))
    band_id = db.Column(db.Integer, db.ForeignKey(Band.band_id))
    event = db.relationship("Event", backref="song")

    def __repr__(self):
        return '<Song {}>'.format(self.song_title)

class Event(db.Model):
    event_id = db.Column(db.Integer, primary_key = True)
    event_name = db.Column(db.String(63))
    event_type = db.Column(db.String(63))
    event_banner = db.Column(db.String(255))
    type_boost = db.Column(db.String(63))
    card = db.relationship("Card", backref="event")
    gacha_id = db.Column(db.Integer, db.ForeignKey("gacha.gacha_id"))
    song_id = db.Column(db.Integer, db.ForeignKey("song.song_id"))

    def __repr__(self):
        return '<Event {}>'.format(self.event_name)

class Gacha(db.Model):
    gacha_id = db.Column(db.Integer, primary_key = True)
    gacha_name = db.Column(db.String(63))
    gacha_type = db.Column(db.String(63))
    gacha_banner = db.Column(db.String(255))
    cards = db.relationship("Card", backref="gacha")
    event = db.relationship("Event", backref="gacha")

    def __repr__(self):
        return '<Gacha {}>'.format(self.gacha_name)

class Card(db.Model):
    card_id = db.Column(db.Integer, primary_key = True)
    char_id = db.Column(db.Integer, db.ForeignKey('character.char_id'))
    card_title = db.Column(db.String(63))
    overall_stat = db.Column(db.Integer)
    untrained_art_link = db.Column(db.String(255))
    trained_art_link = db.Column(db.String(255))
    rarity = db.Column(db.Integer)
    attribute = db.Column(db.String(63))
    gacha_id = db.Column(db.Integer, db.ForeignKey("gacha.gacha_id"))
    event_id = db.Column(db.Integer, db.ForeignKey("event.event_id"))

    def __repr__(self):
        return '<Card {}>'.format(self.card_title)