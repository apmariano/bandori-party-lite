from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, IntegerField
from wtforms.validators import DataRequired, NumberRange

attribute_choice = [("Powerful", "Powerful"), ("Pure", "Pure"), ("Cool", "Cool"), ("Happy", "Happy")]

class CharacterForm(FlaskForm):
    char_name = StringField('Character Name', validators=[DataRequired()])
    submit = SubmitField('Submit')

class SongForm(FlaskForm):
    song_title = StringField('Song Title', validators=[DataRequired()])
    song_length = StringField('Song Length')
    ex_level = IntegerField('EX Level', validators=[DataRequired(), NumberRange(1, 30)])
    note_count = IntegerField('Note Count', validators=[DataRequired()])
    song_art = StringField('Song Art URL', validators=[DataRequired()])
    band_id = SelectField(label="Band", coerce=int)
    submit = SubmitField("Submit")

class EventForm(FlaskForm):
    event_type_choices = [("Regular Event", "Regular Event"), ("Challenge Live", "Challenge Live"), ("VS Live", "VS Live"), ("Trial Live", "Trial Live"), ("Mission Live", "Mission Live")]
    event_name = StringField('Event Name', validators=[DataRequired()])
    event_type = SelectField(label="Event Type", choices=event_type_choices)
    event_banner = StringField('Event Banner Image URL', validators=[DataRequired()])
    type_boost = SelectField(label="Type Boost", choices=attribute_choice)
    gacha_id = SelectField(label="Gacha", coerce=int)
    song_id = SelectField(label="Song", coerce=int)
    submit = SubmitField("Submit")

class GachaForm(FlaskForm):
    gacha_name = StringField('Gacha Name', validators=[DataRequired()])
    gacha_type = SelectField(label="Gacha Type", choices=[("Regular", "Regular"), ("Limited", "Limited")])
    gacha_banner = StringField('Gacha Banner Image URL', validators=[DataRequired()])
    submit = SubmitField("Submit")

class CardForm(FlaskForm):
    char_id = SelectField(label="Character", coerce=int)
    card_title = StringField('Card Title', validators=[DataRequired()])
    overall_stat = IntegerField('Overall Stat', validators=[DataRequired()])
    untrained_art_link = StringField('Untrained Image URL', validators=[DataRequired()])
    trained_art_link = StringField('Trained Image URL')
    rarity = SelectField('Rarity', coerce=int, choices=[(1,"(1) *"),(2,"(2) **"),(3,"(3) ***"),(4,"(4) ****")])
    attribute = SelectField('Attribute', choices=attribute_choice)
    gacha_id = SelectField(label="Gacha", coerce=int)
    event_id = SelectField(label="Event", coerce=int)
    submit = SubmitField('Submit')