"""empty message

Revision ID: 2c853c373a15
Revises: 692905455f30
Create Date: 2018-11-22 19:25:53.864678

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2c853c373a15'
down_revision = '692905455f30'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('card', sa.Column('event_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'card', 'event', ['event_id'], ['event_id'])
    op.add_column('character', sa.Column('gacha_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'character', 'band', ['gacha_id'], ['band_id'])
    op.add_column('event', sa.Column('gacha_id', sa.Integer(), nullable=True))
    op.add_column('event', sa.Column('song_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'event', 'song', ['song_id'], ['song_id'])
    op.create_foreign_key(None, 'event', 'gacha', ['gacha_id'], ['gacha_id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'event', type_='foreignkey')
    op.drop_constraint(None, 'event', type_='foreignkey')
    op.drop_column('event', 'song_id')
    op.drop_column('event', 'gacha_id')
    op.drop_constraint(None, 'character', type_='foreignkey')
    op.drop_column('character', 'gacha_id')
    op.drop_constraint(None, 'card', type_='foreignkey')
    op.drop_column('card', 'event_id')
    # ### end Alembic commands ###
