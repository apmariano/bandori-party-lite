"""empty message

Revision ID: df54c492ffbf
Revises: b47e4b46a104
Create Date: 2018-11-22 12:37:15.720155

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'df54c492ffbf'
down_revision = 'b47e4b46a104'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('card', sa.Column('gacha_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'card', 'gacha', ['gacha_id'], ['gacha_id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'card', type_='foreignkey')
    op.drop_column('card', 'gacha_id')
    # ### end Alembic commands ###
