import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'secret_key_password'
    # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'mysql+pymysql://admin:password@localhost:3306/flask'
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://bb81e6e0fa6584:ed206182@us-cdbr-iron-east-01.cleardb.net/heroku_72a0c14bffa520a'
    SQLALCHEMY_TRACK_MODIFICATIONS = False